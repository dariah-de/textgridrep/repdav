## [1.4.1](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.4.0...1.4.1) (2024-11-22)


### Bug Fixes

* **logging:** apply log level to wsgidav logger ([5d80c80](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/5d80c807d55eea8f692203c05529cf77cc231e0d))

# [1.4.0](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.3.1...1.4.0) (2024-11-21)


### Features

* make service timeout configurable ([a87e464](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/a87e4647ee3e492dcfe8de2e68bc8325e771308f))

## [1.3.1](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.3.0...1.3.1) (2023-12-01)


### Performance Improvements

* define and use global service clients for auth, crud and search ([e0bb98d](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/e0bb98d619d770b2166a6fea09786938b41724d4))

# [1.3.0](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.2.5...1.3.0) (2023-11-30)


### Bug Fixes

* **get_member:** add missing tguri declaration ([b4350c6](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/b4350c623f088c0ba9c2843997de483d4b3407fc))


### Features

* add tgobject metadata caching to aggregations and projects ([4471111](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/447111122a1c0a62c2409322a5c64ea2142de471))
* global cache for project names ([7760b84](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/7760b84a38a2b3532e6516f7f26c981a7dabee7b))

## [1.2.5](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.2.4...1.2.5) (2023-11-28)


### Bug Fixes

* fix list index out of range if tgsearch info request has no authorization ([c3f28b3](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/c3f28b3cbb2ce96491c25600a11b50e49b52333a))

## [1.2.4](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.2.3...1.2.4) (2023-05-11)


### Bug Fixes

* **config:** make logging logging configurable with REPDAV_LOG_LEVEL environment variable ([fa95200](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/fa952000cce935b9319731130cb835e515e7bbfa)), closes [#10](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/issues/10)

## [1.2.3](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.2.2...1.2.3) (2023-04-20)


### Bug Fixes

* **build:** fix k8s compatibility issue and update dependencies ([07d342d](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/07d342d0935ea9730578b5f100e57a150bdc71a9)), closes [#44](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/issues/44)

## [1.2.2](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.2.1...1.2.2) (2023-01-13)


### Bug Fixes

* **TextgridResource:** TextgridCrudRequest is used for low level access without databinding now ([c04f461](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/c04f4616cfcd7e8e7a339ea18f29efaf4068e884))

## [1.2.1](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.2.0...1.2.1) (2023-01-10)


### Bug Fixes

* reduce log level of some debug information ([c38b76b](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/c38b76bc8199f72b1e1bcfb0bae014b605200849))

# [1.2.0](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.1.3...1.2.0) (2023-01-09)


### Features

* **textgrid_dav_provider:** initialize textgrid config with "tg_host" from wsgidav environ ([fab21bc](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/fab21bcf60737f8d710f3e8419195ba10d20139f))

## [1.1.3](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.1.2...1.1.3) (2022-11-29)


### Bug Fixes

* **wsgidav:** update to remediate GHSA-xx6g-jj35-pxjv, CVE-2022-41905 ([a6f91cc](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/a6f91cc20e3a1766d9746f3b10eb08bb31eab85e)), closes [#32](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/issues/32)

## [1.1.2](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.1.1...1.1.2) (2022-02-28)


### Bug Fixes

* **dav_provider:** reference correct class member ([a8f69ac](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/a8f69ac73ca9e1421128a6fcb5896032a288cb6a))

## [1.1.1](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.1.0...1.1.1) (2022-02-25)


### Bug Fixes

* **dockerfile:** move pyicu library build to own docker build stage ([09e5fd1](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/09e5fd1d13a0ed6476a269ce9fd6e90d877972b2))

# [1.1.0](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.0.2...1.1.0) (2022-02-25)


### Bug Fixes

* **dav-provider:** correct log call string interpolation ([42ece56](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/42ece562ca4a9aa5418424b7469c3e4eb608b06a))


### Features

* **textgrid_named_dav_provider.py:** dav provider which shows titles from metadata in filenames and paths ([1520f46](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/1520f464d043ecbe9943819c01183a2bfdec8007))
* **textgrid_named_dav_provider.py:** show project names in dav root ([fe73736](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/fe73736eb22c1b30f1ac0f17460ddba749424eb9))

## [1.0.2](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.0.1...1.0.2) (2022-02-23)


### Bug Fixes

* **wsgidav:** update wsgidav to 4.0.1; remove requests, zeep; rename config key that has changed with the update ([834901e](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/834901e6278f034b4b7c7f1cb2f7ad3fb0a42415))

## [1.0.1](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/compare/1.0.0...1.0.1) (2022-02-22)


### Bug Fixes

* **dav-provider:** use TextgridSearch from tgclients lib instead of own tgapi module ([c88fcaa](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/c88fcaa2b4a907994fdb523bf1558434f96b6669)), closes [#20](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/issues/20)

# 1.0.0 (2022-02-21)


### Bug Fixes

* **errors:** set env_name in error message correctly ([06b0b4d](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/06b0b4d8dee8f553efed4b0f406411047cffae86))
* **textgrid_dav_provider:** use tgclients for `TextgridAuth` ([81c6048](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/81c60487613f5f3e836e2c3383f6237a709e5dba))
* **tgapi:** take textgridUri from metadata, not from result element ([88079e8](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/88079e8c433b010679accaea204bcfd636603adc)), closes [#6](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/issues/6)


### Features

* **config:** add AppConfig with property "dsn" for sentry initialization ([4cebb95](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/4cebb95a48384c2592745090fd03ef20735ebe4f))
* **config:** add properties for host and nav_address ([564bad8](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/564bad89a40ab723586e7350bff5394b785d0788))
* **dav_provider:** provide access to Textgrid projects, aggregations and resources ([bd80a81](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/bd80a81bea5091899b7d7704d19913d843a39f75))
* implement basic textgrid DomainController and DAVProvider ([86859a0](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/86859a0794c0ab3846e7102d5676b94dd3d6706a))
* **main.py:** initialize and launch the wsgi server process ([4fca508](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/4fca5087eb1be9c50eff8b1948dc0565563268ca))
* **main:** init sentry very early if dsn is set ([2c3e76e](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/2c3e76edd04a0a98b5681f730aea044af989cd89))
* provide modules for config, errors and textgrid api ([be42d0f](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/be42d0f89b644b03e70fad4c9a5c945c3bdd5736))
* **stream_tools:** add a modified version of wsgidav's FileLikeQueue ([1b551e9](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/1b551e9bad9b6793c09c3ba722d3a704e6b62d9e))
* **textgrid_dav_provider:** implement `begin_write` for `TextgridResource`s ([5ad6f4d](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/5ad6f4d2e8bf11b03b9f9f6b7280487dd25aaf8b))
* **textgrid_dav_provider:** provide streamed write to resources with FileLikeQueue and threaded worker ([49f8b4f](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/49f8b4f66092f898f376235a2b9a03a88e3e1d1b))
* **tgapi:** provide access to the Textgrid Search and CRUD services ([61ce9f7](https://gitlab.gwdg.de/dariah-de/textgridrep/repdav/commit/61ce9f7a3c5627aa79416108aa5068ef2fd30cd7))
