# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

# syntax=docker/dockerfile:1
FROM docker.io/python:3.10-alpine AS builder

# build icu lib
# hadolint ignore=DL3018
RUN apk add --no-cache g++ icu-dev \
    && pip install --no-cache-dir PyICU==2.14


FROM docker.io/python:3.10-alpine

LABEL \
    org.opencontainers.image.licenses="AGPL-3.0-or-later" \
    org.opencontainers.image.authors="Stefan Hynek" \
    org.opencontainers.image.title="Textgrid Repository WebDAV Server" \
    org.opencontainers.image.url="https://gitlab.gwdg.de/dariah-de/textgridrep/repdav" \
    org.opencontainers.image.source="https://gitlab.gwdg.de/dariah-de/textgridrep/repdav" \
    org.opencontainers.image.vendor="SUB/FE"

# copy pre-compiled icu lib from builder stage
COPY --from=builder /usr/local/lib/python3.10/site-packages/icu /usr/local/lib/python3.10/site-packages/icu
COPY --from=builder /usr/local/lib/python3.10/site-packages/PyICU-2.14.dist-info /usr/local/lib/python3.10/site-packages/PyICU-2.14.dist-info

# icu-dev package is nevertheless needed by the python library
# hadolint ignore=DL3018
RUN adduser -D repdav -u 1000 \
    && apk add --no-cache icu-dev

# use a "numeric" user for k8s compatibility
USER 1000

COPY --chown=repdav requirements.txt /
RUN pip install \
    --no-cache-dir \
    --requirement requirements.txt \
    --user

WORKDIR /app
COPY --chown=repdav src/ .

ARG build_date
ARG vcs_ref
ARG version
LABEL \
    org.opencontainers.image.created="${build_date}" \
    org.opencontainers.image.revision="${vcs_ref}" \
    org.opencontainers.image.version="${version}"

CMD ["python", "main.py"]

COPY Dockerfile /
