<!--
SPDX-FileCopyrightText: 2022 Stefan Hynek

SPDX-License-Identifier: CC0-1.0
-->

# repdav

## Development

1. Prerequisites
    - Python > 3.8
    - Docker > 20.10
    - Docker Compose > 1.25
    - libicu-dev

1. Create/activate virtual environment (ensure you use the correct python version!).

    ```sh
    python -m venv venv
    . venv/bin/activate
    ```

1. Install requirements.

    ```sh
    pip install -r requirements.txt -r requirements.dev.txt
    ```

1. Configure the service by setting the following variables in your environment.

    Name | Usage / Default
    ------|-----------------
    **TEXTGRID_URL** | Textgrid repository service URL. Default: **<https://textgridlab.org>**
    **REPDAV_HOST** | Service hostname. Default: **localhost**
    **REPDAV_LOG_LEVEL** | Service logging level. Default: **WARNING**
    **REPDAV_PORT** | Service port. Default: **8080**
    **SENTRY_DSN** | Sentry Data Source Name for the Service. If unset, disables Sentry Error Tracking. Default: ***None***
    **SENTRY_ENV** | Sentry environment. Default: **production**

1. Start the service.

    ```sh
    python src/main.py
    ```

-----
Attention: This setup assumes you have the tgclients library source code installed at `../tgclients/`; see the [development requirements](requirements.dev.txt).

-----

For your convenience, a Docker Compose setup is delivered with this repository that configures the service accordingly. Build and start the stack.

```sh
docker-compose up --build
```

This setup does not rely on a local installation of the tgclients library but instead installs the (supposedly) latest and tested compatible version of the library; see the [requirements](requirements.txt).

## Contributing

Commit convention:

- Use [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)

Style constraints:

- Code: [PEP 8](https://www.python.org/dev/peps/pep-0008/)
- Documentation: [PEP 287](https://www.python.org/dev/peps/pep-0287/)

Coding constraints:

- Objects that are not supposed to be used outside the current scope MUST be named starting with `_` (underscore): [PEP 316](https://www.python.org/dev/peps/pep-0316/#id12)

For your convenience, pre-commit hooks are configured to check against these constraints. Provided, you have installed the development requirements (see above), activate `pre-commit` to run on every `git commit`:

```sh
pre-commit install
```

Also, a helper with conventional commits is installed with the development requirements that you could leverage to easily comply with it. Just use `cz c` instead of `git commit`

## License

This project aims to be [REUSE compliant](https://api.reuse.software/info/gitlab.gwdg.de/dariah-de/textgridrep/repdav).
Original parts are licensed under AGPL-3.0-or-later.
Derivative code is licensed under the respective license of the original.
Documentation, configuration and generated code files are licensed under CC0-1.0.

## Badges

[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/dariah-de/textgridrep/repdav)](https://api.reuse.software/info/gitlab.gwdg.de/dariah-de/textgridrep/repdav)
