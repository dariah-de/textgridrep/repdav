# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import logging

from wsgidav.dc.base_dc import BaseDomainController

# see https://github.com/mar10/wsgidav/blob/master/wsgidav/dc/base_dc.py
# for the implementation of the BaseDomainController

_logger = logging.getLogger(__name__)


# see https://github.com/mar10/wsgidav/blob/master/wsgidav/dc/simple_dc.py
# for the implementation of the SimpleDomainController
class TextgridDC(BaseDomainController):
    def __init__(self, wsgidav_app, config):
        super(TextgridDC, self).__init__(wsgidav_app, config)
        return

    def __str__(self):
        return "{}()".format(self.__class__.__name__)

    def get_domain_realm(self, path_info, environ):
        """Resolve a relative url to the appropriate realm name."""
        realm = self._calc_realm_from_path_provider(path_info, environ)
        _logger.debug("REALM: %s", realm)
        return realm

    def require_authentication(self, realm, environ):
        """Authentication with a valid session ID as the username is required."""
        return True

    def basic_auth_user(self, realm, user_name, password, environ):
        """Authentication will always be granted in the first place because sid
        validation is an "expensive" request.
        """
        return True

    def supports_http_digest_auth(self):
        return False
