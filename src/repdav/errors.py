# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

"""Custom error classes
"""
import logging

_logger = logging.getLogger(__name__)


class _ConfigError(Exception):
    """Base class for config errors. Do not import directly."""

    pass


class EnvNotSetError(_ConfigError):
    """Exception to raise if an environment variable is not set (correctly)."""

    def __init__(self, env_name):
        self.env_name = env_name
        self.message = f"Environment variable {env_name} is not set."
        _logger.error(self.message, self.env_name)
        super().__init__(self.message)
