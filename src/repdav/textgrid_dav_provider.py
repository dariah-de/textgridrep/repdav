# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""The DAV resource provider and its resources.
"""
import io
import logging
import threading
from pprint import pformat

from tgclients import TextgridAuth, TextgridCrudRequest, TextgridSearch
from wsgidav.dav_provider import DAVCollection, DAVNonCollection, DAVProvider
from wsgidav.util import join_uri, pop_path

from repdav.config import tg_conf
from repdav.stream_tools import FileLikeQueue

_logger = logging.getLogger(__name__)


# Textgrid service clients are instantiated once globally and reused across
# all webdav requests. This saves time for tgauth soap client creation and
# allows reusing tcp connections.
tg_auth = TextgridAuth(tg_conf)
tg_search = TextgridSearch(tg_conf, nonpublic=True)
tg_crud = TextgridCrudRequest(tg_conf)


class TextgridCachedObjectsMixin:
    """Metadata caching methods for listing projects and aggregations.
    Requires `self._cache: dict` to be implemented in the target class.
    """

    def _get_tgobject_metadata(self, tguri, sid):
        if tguri not in self._cache:
            _logger.debug("no cache hit for %s", tguri)
            response = tg_search.info(tguri, sid)
            self._cache_tgobject_metadata(response.result[0])

        return self._cache[tguri]

    def _cache_tgobject_metadata(self, metadata):
        tguri = metadata.object_value.generic.generated.textgrid_uri.value
        if tguri not in self._cache:
            _logger.debug("storing metadata for %s", tguri)
            self._cache[tguri] = metadata


class TextgridRoot(DAVCollection):
    """Top level collection that incorporates Textgrid projects.

    This is implemented as READ-ONLY as projects may not be created with webDAV.
    """

    def __init__(self, path, environ):
        DAVCollection.__init__(self, path, environ)
        self._sid = environ["wsgidav.auth.user_name"]
        self.projects = ()

    def get_display_info(self):
        return {"type": "Textgrid root collection"}

    def get_member_names(self):
        _logger.debug("Called TextgridRoot.get_member_names(self).")
        projects = tuple(tg_auth.list_assigned_projects(self._sid))
        _logger.debug("MY PROJECTS: %s", projects)
        return projects

    def get_member(self, name):
        _logger.debug("Called TextgridRoot.get_member(self, %s).", name)
        return TextgridProject(join_uri(self.path, name), self.environ)

    def support_etag(self):
        """Return True, if this resource supports ETags."""
        return False

    def get_etag(self):
        """
        See http://www.webdav.org/specs/rfc4918.html#PROPERTY_getetag

        This method SHOULD be implemented, especially by non-collections.
        """
        return None

    # temporary override for debugging
    def resolve(self, script_name, path_info):
        """Return a _DAVResource object for the path (None, if not found).
        `path_info`: is a URL relative to this object.
        """
        _logger.debug(
            "Called TextgridRoot.resolve(self, %s, %s).", script_name, path_info
        )
        if path_info in ("", "/"):
            return self
        name, rest = pop_path(path_info)
        res = self.get_member(name)
        _logger.debug("TextgridRoot_NAME: %s, REST: %s, RES: %s", name, rest, res)
        if res is None or rest in ("", "/"):
            return res
        _logger.debug("RES: %s", res)
        return res.resolve(join_uri(script_name, name), rest)


class TextgridProject(DAVCollection, TextgridCachedObjectsMixin):
    def __init__(self, path, environ):
        _logger.debug("Called TextgridProject.__init__(self, %s, environ).", path)
        DAVCollection.__init__(self, path, environ)
        self._sid = environ["wsgidav.auth.user_name"]
        self._project_id = self.path.split("/")[-1]
        self._cache = {}

    def create_empty_resource(self, name):
        pass

    def create_collection(self, name):
        pass

    def get_display_info(self):
        return {"type": "Textgrid project"}

    def get_member_names(self):
        _logger.debug("Called TextgridProject.get_member_names(self).")
        response = tg_search.list_project_root(self._project_id, self._sid)
        names = []
        for result in response.result:
            names.append(result.object_value.generic.generated.textgrid_uri.value)
            self._cache_tgobject_metadata(result)
        return names

    def get_member(self, name):
        _logger.debug("Called TextgridProject.get_member(self, %s).", name)
        tguri = name
        meta = self._get_tgobject_metadata(tguri, self._sid)
        if meta.authorized is False:
            _logger.debug("unauthorized: %s", tguri)
            info = {
                name: {
                    "title": "Restricted TextGrid Object",
                    "format": "unknown",
                    "extent": 0,
                }
            }
        else:
            info = {
                name: {
                    "title": meta.object_value.generic.provided.title,
                    "format": meta.object_value.generic.provided.format,
                    "extent": meta.object_value.generic.generated.extent,
                }
            }
        _logger.info("INFO: %s", info)
        if "aggregation" in info[name]["format"]:
            return TextgridAggregation(join_uri(self.path, name), self.environ, info)
        return TextgridResource(join_uri(self.path, name), self.environ, info)

    def delete(self):
        pass

    def copy_move_single(
        self, dest_path, is_move
    ):  # pylint: disable=W0221:arguments-differ
        pass

    def support_etag(self):
        """Return True, if this resource supports ETags."""
        return False

    def get_etag(self):
        """
        See http://www.webdav.org/specs/rfc4918.html#PROPERTY_getetag

        This method SHOULD be implemented, especially by non-collections.
        """
        return None

    # temporary override for debugging
    def resolve(self, script_name, path_info):
        """Return a _DAVResource object for the path (None, if not found).
        `path_info`: is a URL relative to this object.
        """
        _logger.debug(
            "Called TextgridProject.resolve(self, %s, %s).", script_name, path_info
        )
        if path_info in ("", "/"):
            return self
        name, rest = pop_path(path_info)
        res = self.get_member(name)
        _logger.debug("TextgridProject_NAME: %s, REST: %s, RES: %s", name, rest, res)
        if res is None or rest in ("", "/"):
            return res
        return res.resolve(join_uri(script_name, name), rest)


# TODO: merge TextgridProject with TextgridAggregation
# and/or derive from a common base class.


class TextgridAggregation(DAVCollection, TextgridCachedObjectsMixin):
    def __init__(self, path, environ, info):
        _logger.debug(
            "Called TextgridAggregation.__init__(self, %s, environ, info).", path
        )
        DAVCollection.__init__(self, path, environ)
        self._sid = environ["wsgidav.auth.user_name"]
        self._info = info
        self._tguri = self.path.split("/")[-1]
        self._cache = {}

    def create_empty_resource(self, name):
        pass

    def create_collection(self, name):
        pass

    def get_display_info(self):
        return {"type": "Textgrid aggregation"}

    def get_member_names(self):
        _logger.debug("Called TextgridAggregation.get_member_names(self).")
        response = tg_search.list_aggregation(self._tguri, self._sid)
        names = []
        for result in response.result:
            names.append(result.object_value.generic.generated.textgrid_uri.value)
            self._cache_tgobject_metadata(result)
        return names

    def get_member(self, name):
        _logger.debug("Called TextgridAggregation.get_member(self, %s).", name)
        tguri = name
        meta = self._get_tgobject_metadata(tguri, self._sid)
        if meta.authorized is False:
            _logger.debug("unauthorized: %s", tguri)
            info = {
                name: {
                    "title": "Restricted TextGrid Object",
                    "format": "unknown",
                    "extent": 0,
                }
            }
        else:
            info = {
                name: {
                    "title": meta.object_value.generic.provided.title,
                    "format": meta.object_value.generic.provided.format,
                    "extent": meta.object_value.generic.generated.extent,
                }
            }
        _logger.info("INFO: %s", info)
        if "aggregation" in info[name]["format"]:
            return TextgridAggregation(join_uri(self.path, name), self.environ, info)
        return TextgridResource(join_uri(self.path, name), self.environ, info)

    def delete(self):
        pass

    # See [Issue](https://github.com/pylint-dev/pylint/issues/5793) why this disabled from linting.
    def copy_move_single(
        self, dest_path, is_move
    ):  # pylint: disable=W0221:arguments-differ
        pass

    def support_etag(self):
        """Return True, if this resource supports ETags."""
        return False

    def get_etag(self):
        """
        See http://www.webdav.org/specs/rfc4918.html#PROPERTY_getetag

        This method SHOULD be implemented, especially by non-collections.
        """
        return None

    # temporary override for debugging
    def resolve(self, script_name, path_info):
        """Return a _DAVResource object for the path (None, if not found).
        `path_info`: is a URL relative to this object.
        """
        _logger.debug(
            "Called TextgridAggregation.resolve(self, %s, %s).", script_name, path_info
        )
        if path_info in ("", "/"):
            return self
        name, rest = pop_path(path_info)
        res = self.get_member(name)
        _logger.debug(
            "TextgridAggregation_NAME: %s, REST: %s, RES: %s", name, rest, res
        )
        if res is None or rest in ("", "/"):
            return res
        return res.resolve(join_uri(script_name, name), rest)


class TextgridResource(DAVNonCollection):
    """Non-Aggregation resources."""

    def __init__(self, path, environ, info):
        _logger.debug("Called TextgridResource.__init__(self, %s, environ).", path)
        DAVNonCollection.__init__(self, path, environ)
        self._size = environ.get("CONTENT_LENGTH")
        self._sid = environ["wsgidav.auth.user_name"]
        self._tguri = self.path.split("/")[-1]
        self._info = info
        self.upload_thread = None

    def get_content_length(self):
        _logger.debug("Called TextgridResource.get_content_length(self).")
        return self._info[self.name]["extent"]

    def get_content_type(self):
        _logger.debug("Called TextgridResource.get_content_type(self).")
        return self._info[self.name]["format"]

    def get_content(self):
        _logger.debug(
            "Called TextgridResource.get_content(self) with path: %s", self.path
        )
        return io.BytesIO(tg_crud.read_data(self._tguri, self._sid).content)

    def get_content_title(self):
        _logger.debug("Called TextgridResource.get_content_title(self).")
        return self._info[self.name]["title"]

    def begin_write(self, content_type=None):  # pylint: disable=W0221:arguments-differ
        _logger.debug(
            "Called TextgridResource.begin_write(self, content_type=%s).", content_type
        )

        queue = FileLikeQueue(int(self._size))
        metadata = tg_crud.read_metadata(self._tguri, self._sid).content

        def worker():
            _logger.debug("Called TextgridResource.begin_write.worker().")
            tg_crud.update_resource(self._sid, self._tguri, queue, metadata)

        thread = threading.Thread(target=worker)
        thread.daemon = True
        thread.start()
        self.upload_thread = thread
        return queue

    def end_write(self, with_errors):  # pylint: disable=W0221:arguments-differ
        _logger.debug(
            "Called TextgridResource.end_write(self, with_errors=%s)", with_errors
        )
        if self.upload_thread:
            self.upload_thread.join()
            self.upload_thread = None

    def support_etag(self):
        """Return True, if this resource supports ETags."""
        return False

    def get_etag(self):
        """
        See http://www.webdav.org/specs/rfc4918.html#PROPERTY_getetag

        This method SHOULD be implemented, especially by non-collections.
        """
        return None

    # temporary override for debugging
    def resolve(self, script_name, path_info):
        """Return a _DAVResource object for the path (None, if not found).
        `path_info`: is a URL relative to this object.
        """
        _logger.debug(
            "Called TextgridResource.resolve(self, %s, %s).", script_name, path_info
        )
        if path_info in ("", "/"):
            return self
        return None


# ============================================================================
# TextgridResourceProvider
# ============================================================================
class TextgridResourceProvider(DAVProvider):
    """DAV provider that serves Textgrid resources."""

    def get_resource_inst(self, path, environ):
        _logger.debug(
            "Called TextgridResourceProvider.get_resource_inst(self, %s, %s).",
            path,
            pformat(environ),
        )
        self._count_get_resource_inst += 1
        root = TextgridRoot("/", environ)
        # an instance of _DAVResource (i.e. either DAVCollection or DAVNonCollection)
        return root.resolve("/", path)
