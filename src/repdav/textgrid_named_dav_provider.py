# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import logging
from functools import lru_cache
from pprint import pformat

from tgclients import TextgridMetadata
from wsgidav.util import join_uri

from repdav.textgrid_dav_provider import (TextgridAggregation, TextgridProject,
                                          TextgridResource,
                                          TextgridResourceProvider,
                                          TextgridRoot, tg_auth, tg_search)

_logger = logging.getLogger(__name__)


@lru_cache(maxsize=1024)
def get_project_name(project_id):
    """project names in tgauth are public and never change, we can cache them indefinitely"""
    return tg_auth.get_project_description(project_id).name + " [" + project_id + "]"


# ============================================================================
# TextgridNamedResourceProvider
# ============================================================================
class TextgridNamedResourceProvider(TextgridResourceProvider):
    """DAV provider that serves Textgrid resources."""

    def get_resource_inst(self, path, environ):
        _logger.debug(
            "Called TextgridNamedResourceProvider.get_resource_inst(self, %s, %s).",
            path,
            pformat(environ),
        )
        self._count_get_resource_inst += 1
        root = TextgridNamedRoot("/", environ)
        # an instance of _DAVResource (i.e. either DAVCollection or DAVNonCollection)
        return root.resolve("/", path)


class TextgridNamedRoot(TextgridRoot):
    """Top level collection that incorporates Textgrid projects.

    This is implemented as READ-ONLY as projects may not be created with webDAV.
    """

    def __init__(self, path, environ):
        super().__init__(path, environ)

    def get_member_names(self):
        _logger.debug("Called TextgridNamedRoot.get_member_names(self).")
        projects = []
        for project in tuple(tg_auth.list_assigned_projects(self._sid)):
            projects.append(get_project_name(project))
        _logger.debug("MY PROJECTS: %s", projects)
        return projects

    def get_member(self, name):
        _logger.debug("Called TextgridRoot.get_member(self, %s).", name)
        return TextgridNamedProject(join_uri(self.path, name), self.environ)


class TextgridNamedProject(TextgridProject):
    def __init__(self, path, environ):
        _logger.debug("Called TextgridNamedProject.__init__(self, %s, environ).", path)
        super().__init__(path, environ)
        self._tgmeta = TextgridMetadata()
        name = self.path.split("/")[-1]
        # the projectID is found between square brackets at the end of the name string
        self._project_id = name[name.rfind("[") + 1 : name.rfind("]")]

    def get_member_names(self):
        _logger.debug("Called TextgridNamedProject.get_member_names(self).")
        response = tg_search.list_project_root(self._project_id, self._sid)
        names = []
        for result in response.result:
            names.append(self._tgmeta.filename_from_metadata(result))
            self._cache_tgobject_metadata(result)
        return names

    def get_member(self, name):
        _logger.debug("Called TextgridNamedProject.get_member(self, %s).", name)
        tguri = "textgrid:" + self._tgmeta.id_from_filename(name)
        meta = self._get_tgobject_metadata(tguri, self._sid)
        if meta.authorized is False:
            _logger.debug("unauthorized: %s", tguri)
            info = {
                name: {
                    "title": "Restricted TextGrid Object",
                    "format": "unknown",
                    "extent": 0,
                }
            }
        else:
            info = {
                name: {
                    "title": meta.object_value.generic.provided.title,
                    "format": meta.object_value.generic.provided.format,
                    "extent": meta.object_value.generic.generated.extent,
                }
            }
        _logger.debug("INFO: %s", info)
        if "aggregation" in info[name]["format"]:
            return TextgridNamedAggregation(
                join_uri(self.path, name), self.environ, info, self._tgmeta
            )
        return TextgridNamedResource(
            join_uri(self.path, name), self.environ, info, self._tgmeta
        )


class TextgridNamedAggregation(TextgridAggregation):

    def __init__(self, path, environ, info, tgmeta):
        _logger.debug(
            "Called TextgridNamedAggregation.__init__(self, %s, environ, info).", path
        )
        super().__init__(path, environ, info)
        self._tgmeta = tgmeta
        self._tguri = "textgrid:" + self._tgmeta.id_from_filename(
            self.path.split("/")[-1]
        )

    def get_member_names(self):
        _logger.debug("Called TextgridNamedAggregation.get_member_names(self).")
        response = tg_search.list_aggregation(self._tguri, self._sid)
        names = []
        for result in response.result:
            names.append(self._tgmeta.filename_from_metadata(result))
            self._cache_tgobject_metadata(result)
        return names

    def get_member(self, name):
        _logger.debug("Called TextgridNamedAggregation.get_member(self, %s).", name)
        tguri = "textgrid:" + self._tgmeta.id_from_filename(name)
        meta = self._get_tgobject_metadata(tguri, self._sid)
        if meta.authorized is False:
            print("auth false")
            info = {
                name: {
                    "title": "Restricted TextGrid Object",
                    "format": "unknown",
                    "extent": 0,
                }
            }
        else:
            info = {
                name: {
                    "title": meta.object_value.generic.provided.title,
                    "format": meta.object_value.generic.provided.format,
                    "extent": meta.object_value.generic.generated.extent,
                }
            }
        _logger.debug("INFO: %s", info)
        if "aggregation" in info[name]["format"]:
            return TextgridNamedAggregation(
                join_uri(self.path, name), self.environ, info, self._tgmeta
            )
        return TextgridNamedResource(
            join_uri(self.path, name), self.environ, info, self._tgmeta
        )


class TextgridNamedResource(TextgridResource):
    """Non-Aggregation resources."""

    def __init__(self, path, environ, info, tgmeta):
        _logger.debug("Called TextgridNamedResource.__init__(self, %s, environ).", path)
        super().__init__(path, environ, info)
        self._tgmeta = tgmeta
        self._tguri = "textgrid:" + self._tgmeta.id_from_filename(
            self.path.split("/")[-1]
        )
