# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import logging
import os
from urllib.parse import urlparse

from tgclients import TextgridConfig

_logger = logging.getLogger(__name__)

DEFAULT_TIMEOUT = 60


def check_url(url: str):
    """Check if `url` can be parsed."""
    result = urlparse(url)
    if result.scheme and result.netloc:
        return url
    raise ValueError(f"{url} is not a valid URL.")


def check_port(port: int):
    """Check if port is set correctly to a non-privileged value."""
    if 1000 <= port <= 65535:
        return port
    raise ValueError(f"{port} is not a valid Port.")


def check_log_level(level: str):
    """Check if log level exists."""
    if level in logging._nameToLevel:  # pylint: disable=protected-access
        return level
    _logger.warning("%s is not a valid Logging Level. Falling back to WARNING.", level)
    return "WARNING"


class AppConfig:
    """Settings that can be be configured from the environment."""

    def __init__(self) -> None:
        _host = os.getenv("REPDAV_HOST")
        self._host = _host if _host else "localhost"
        _log_level = os.getenv("REPDAV_LOG_LEVEL")
        self._log_level = check_log_level(_log_level) if _log_level else "WARNING"
        _port = os.getenv("REPDAV_PORT")
        self._port = check_port(int(_port)) if _port else 8080
        _tg_host = os.getenv("TEXTGRID_URL")
        self._tg_host = check_url(_tg_host) if _tg_host else "https://textgridlab.org"
        _timeout = os.getenv("REPDAV_TIMEOUT", str(DEFAULT_TIMEOUT))
        self._timeout = int(_timeout) if _timeout.isdigit() else DEFAULT_TIMEOUT

    @property
    def host(self) -> str:
        """Service hostname."""
        return self._host

    @property
    def log_level(self) -> str:
        """Service log level."""
        return self._log_level

    @property
    def port(self) -> int:
        """Service port."""
        return self._port

    @property
    def tg_host(self) -> str:
        """Textgrid host URL."""
        return self._tg_host

    @property
    def timeout(self) -> int:
        """Timeout setting for both the repdav and the textgrid service."""
        return self._timeout


app_config = AppConfig()

tg_conf = TextgridConfig(app_config.tg_host)
tg_conf.http_timeout = app_config.timeout
