# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

"""Configure and start the WSGI DAV Server."""
import logging

import sentry_sdk
import sentry_sdk.utils
from cheroot import wsgi
from wsgidav.wsgidav_app import WsgiDAVApp

from repdav.config import app_config

logging.basicConfig(
    level=app_config.log_level, format="%(name)s %(levelname)s %(asctime)s %(message)s"
)

try:
    sentry_sdk.init()
except sentry_sdk.utils.BadDsn as err:
    logging.error("Sentry SDK initialization failed because of invalid DSN: %s", err)

_logger = logging.getLogger(__name__)
_logger.propagate = True

# configure the "wsgidav" named logger
wsgidav_logger = logging.getLogger("wsgidav")
wsgidav_logger.propagate = True
wsgidav_logger.setLevel(app_config.log_level)

# Configuration of the WsgiDAVApp.
# https://wsgidav.readthedocs.io/en/latest/user_guide_configure.html
config = {
    "host": app_config.host,
    "port": app_config.port,
    "provider_mapping": {
        "/": {
            "class": "repdav.textgrid_named_dav_provider.TextgridNamedResourceProvider"
        },
    },
    "http_authenticator": {
        "domain_controller": "repdav.textgrid_domain_controller.TextgridDC",
        "accept_basic": True,  # Allow basic authentication, True or False
        "accept_digest": False,  # Allow digest authentication, True or False
        # True (default digest) or False (default basic)
        "default_to_digest": False,
        # Name of a header field that will be accepted as authorized user
        "trusted_auth_header": None,
    },
    "tg_host": app_config.tg_host,
}

app = WsgiDAVApp(config)

server_args = {
    "bind_addr": (config["host"], config["port"]),
    "timeout": app_config.timeout,
    "wsgi_app": app,
}
server = wsgi.Server(**server_args)
server.start()
